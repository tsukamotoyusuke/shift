Rails.application.routes.draw do
  
  devise_for :users

  resources :users, only: [:show]  # 追加
  resources :tweets   # この行を追加
  get 'likes/create'

  get 'likes/destroy'

  root 'static_pages#home'
  

  resources :tweets do
    post 'like' => 'likes#create'
    delete 'like' => 'likes#destroy'

  end
  
  resources :users, only: [:show] do
    get 'likes' => 'likes#index'
  end

end