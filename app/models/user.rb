class User < ApplicationRecord
  has_many :likes #追加
  has_many :tweets  # 追加
  has_many :liked_tweets, :through => :likes, :source => :tweet  # 追加

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  def liked_count
    likes.size
  end
end
