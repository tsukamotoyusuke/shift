class TweetsController < ApplicationController
  def index
    @tweet = Tweet.new  # 追加
    @tweets = Tweet.all
  end

  # 以下を追加
  def create
    @tweet = current_user.tweets.build(tweet_params)
    @tweet.save
    redirect_to tweets_path
  end

   # 追加する
  def destroy
    @tweet = Tweet.find(params[:id])
    @tweet.destroy
    redirect_to tweets_path
  end

  private
    def tweet_params
      params.require(:tweet).permit(:content)
    end
end