class LikesController < ApplicationController
  
  def index
    @user = User.find(params[:user_id])
    @tweets = @user.liked_tweets

    render "users/show"  # 追加（Like#indexでUsers#showのテンプレートを使用する）
  end

  def create
    tweet = Tweet.find(params[:tweet_id])
    tweet.like(current_user.id)

    redirect_back(fallback_location: root_path)

  end

  def destroy

    tweet = Tweet.find(params[:tweet_id])
    tweet.unlike(current_user.id)

    redirect_back(fallback_location: root_path)

  end
end
